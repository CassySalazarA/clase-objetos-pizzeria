/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.Cassy;

/**
 *
 * @author CassySalazar
 */
//metodo Main es el que permitirá instanciar | debe ser siempre estatico
//psvm: public static void main |
// la clase para a ser un tipo de dato

//public class Pizzeria {
//   public static void main(String[] args) {
//    03 instanciar objetos   
//        Pizza pizza1 = new Pizza();
//        System.out.println(pizza1.nombre);
//        pizza1.nombre = "Española";
//        System.out.println(pizza1.nombre);
//     Es una mala practica, los atributos deben ir encapsulados
     
 public class Pizzeria {
    public static void main(String[] args) {
     //03 instanciar objetos   
        Pizza pizza1 = new Pizza();
        pizza1.getNombre();
        System.out.println(pizza1.getNombre());
        pizza1.setNombre("Española");
        System.out.println(pizza1.getNombre());
        
        Pizza pizza2= new Pizza("Pepperoni","Familiar","Normal");
        System.out.println("Pizza2: "+ pizza2.toString());
        
        //7 cambiar el dato de un atributo
        
        pizza2.setNombre("vegetariana");
        System.out.println(pizza2.toString());
        
        
        Pizza pizza3= new Pizza("Hawaiana", "Familiar", "Borde Queso");
        
        pizza3.preparar();
        pizza3.calentar();
        
        System.out.println("Nombre Pizza: " + pizza3.getNombre());
        System.out.println("Tamaño Pizza: "+ pizza3.getTamano());
        System.out.println("Masa Pizza: "+ pizza3.getMasa());
        System.out.println(pizza3.toString());
        
        
        
     
     
  
    }


    
    
    
    
}
