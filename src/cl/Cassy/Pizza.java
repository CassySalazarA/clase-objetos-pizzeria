/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.Cassy;

/**
 *
 * @author CassySalazar
 */
public class Pizza {
    //01 crear atributos
    private String nombre, tamano, masa;
    
    
    //02 contruir metodos (customer) / void = sin retorno | solo manda un mensaje, no espera argumentos ni entrega retorno
    //encapsulamiento(private no permite usar los atributos en otra clase) 
    //public permite usar ese método en cualquier clase 
    //private es un metodo o atributo solo para esa clase
    public void preparar() {
        System.out.println("Estamos preparando tu pizza...\n");    
    }
    public void calentar (){
        System.out.println("Estamos calentando tu pizza...\n");
        
    }
        //04 crear constructor con y sin parametros click derecho insert code constructor
    public Pizza() {
    }

    public Pizza(String nombre, String tamano, String masa) {
        this.nombre = nombre;
        this.tamano = tamano;
        this.masa = masa;
    }
    
    //05 crear metodos setter(editarlo) y getter(obtener) click derecho insert code getter y setter

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTamano() {
        return tamano;
    }

    public void setTamano(String tamano) {
        this.tamano = tamano;
    }

    public String getMasa() {
        return masa;
    }

    public void setMasa(String masa) {
        this.masa = masa;
    }
    //6 crear metodo toString

    @Override
    public String toString() {
        return "Pizza{" + "nombre=" + nombre + ", tamano=" + tamano + ", masa=" + masa + '}';
    }
    
}
